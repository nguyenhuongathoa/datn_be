package com.example.demo.common;

import com.example.demo.exception.RestApiException;

public class AssertUtils {

	public static void assertFalse(boolean expression, String code, String message) {
		if(expression) {
		 throw new RestApiException(code, message);
		}
	}
}
