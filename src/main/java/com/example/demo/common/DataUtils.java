package com.example.demo.common;

public class DataUtils {

	public static boolean notnullObject(Object o) {
		return o != null;
	}
	public static boolean nullObject(Object o) {
		return o == null;
	}
	public static boolean nullOrEmpty(String cs) {
		if(cs.equals(null)||cs.length()==0||cs.trim().length()==0) {
			return true;
		}
		return false;
	}
}
