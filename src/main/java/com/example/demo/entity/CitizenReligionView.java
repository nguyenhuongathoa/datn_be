package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//CITIZEN_RELIGION_VIEW
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@Entity
@Table(name = "CITIZEN_RELIGION_VIEW")
public class CitizenReligionView {

	@Id
	@Column(name = "NAME")
	private String Name;
	

	@Column(name = "CITIZEN_NUMBER")
	private Integer citizenNumber;
}
