package com.example.demo.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.stereotype.Service;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity extends BaseCreateEntity {

	@Column(name = "UPDATE_DATE", columnDefinition = "TIMESTAMP")
	@LastModifiedDate
	private LocalDateTime updateDate;
	
	@Column(name = "UPDATE_BY")
	private String updateBy;
	
	public BaseEntity update(String modifiedBy) {
		this.updateBy = modifiedBy;
		return this;
	}
	public BaseEntity lastModifyInfo(String modifiedBy, boolean isUpdate) {
		if(isUpdate) {
			update(modifiedBy);
		}else {
			create(modifiedBy);
			update(modifiedBy);
		}
		return this;
	}
	@Override
	public BaseEntity autoCreateOrUpdate(String modifiedBy) {
		if(this.getId()==null|| this.getId()==0) {
			create(modifiedBy);
			update(modifiedBy);
		}else {
			update(modifiedBy);
		}
		return this;
	}
}
