package com.example.demo.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@Entity
@Table(name = "CITIZEN_HOUSE_VIEW")
public class CitizenHouseView  extends BaseEntity{

	@Id
    @Column(name = "CITIZEN_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name = "FULL_NAME")
	private String fullName;
	
	@Column(name = "DATE_OF_BIRTH", columnDefinition = "TIMESTAMP")
	@LastModifiedDate
	private LocalDateTime dateOfBirth;
	
	@Column(name = "SEX")
	private String sex;
	
	@OneToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SEX", insertable = false, updatable = false)
	private ReferenceData sexRef;
	
	
	@Column(name = "IDENTIFICATIONN_TYPE")
	private String indentificationType;
	
	@OneToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "IDENTIFICATIONN_TYPE", insertable = false, updatable = false)
	private ReferenceData indentificationTypeRef;
	
	
	@Column(name = "IDENTIFICATIONN_NUMBER")
	private String indentificationNumber;
	
	@Column(name = "PASSPORT")
	private String passport;
	
	@Column(name = "PHONE_NUMBER")
	private String phoneNumber;
	
	@Column(name = "EMAIL")
	private String email;
	
	@Column(name = "NATION")
	private String nation;
	
	@Column(name = "RELIGION")
	private String religion;
	
	@Column(name = "COUNTRY_NAME")
	private String countyName;
	
	@Column(name = "REGION_BIRTH")
	private String regionBirth;
	
	@Column(name = "RESIDENT_ADDRESS")
	private String residentAddress;
	
	@Column(name = "TEMPORARY_RESIDENCE_ADDRESS")
	private String temporaryResidenceAddress;
	
	@Column(name = "EDUCATION")
	private String education;
	
	@Column(name = "EDUCATION_GRADUATE")
	private String educationGraduate;
	
	@Column(name = "SPECIALIZED")
	private String speciallized;
	
	@Column(name = "JOB")
	private String job;
	
	@Column(name = "STATUS_ALIVE")
	private Boolean statusAlive;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "VACCINATION")
	private Integer vaccination;
	
	@Column(name = "HOUSEHOLE_ID")
	private Long houseHoldId;
	
	@Column(name = "HOST_ID")
	private Long hostId;
	
	@Column(name = "HOST_NAME")
	private String hostName;

}

