package com.example.demo.entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@Entity
@Table(name = "HOUSEHOLD_CITIZEN")
public class HouseHoldCitizen {

	@Id
	@Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
    @Column(name = "CITIZEN_ID")
	private Long citizenId;
    
    @Column(name = "HOUSEHOLE_ID")
	private Long houseHoldId;
    
    @Column(name = "RELAIONSHIP_HOUSEHOLD")
    private String relationShipHouseHold;
    
    //trạng thái dang ở dạng nào
    @Column(name = "STATUS_TYPE")
    private String statusType;
    
    //còn ở đấy hay không
    @Column(name ="IS_ACTIVE")
    private Boolean isactive;
    
    @ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CITIZEN_ID", insertable = false, updatable = false)
	private Citizen citizen;
	
   
}
