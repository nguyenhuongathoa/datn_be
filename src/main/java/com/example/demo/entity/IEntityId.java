package com.example.demo.entity;

public interface IEntityId {
 Long getId();
 void setId(Long id);
}
