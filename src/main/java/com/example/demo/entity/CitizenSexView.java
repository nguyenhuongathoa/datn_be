package com.example.demo.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@Entity
@Table(name = "CITIZEN_SEX_VIEW")
public class CitizenSexView {

	@Id
	@Column(name = "NAME")
	private String Name;
	

	@Column(name = "CITIZEN_NUMBER")
	private Integer citizenNumber;

}
