package com.example.demo.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//CITIZEN_EDUCATION_VIEW
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@Entity
@Table(name = "CITIZEN_EDUCATION_VIEW")
public class CitizenEducationView {

	@Id
	@Column(name = "NAME")
	private String Name;
	

	@Column(name = "CITIZEN_NUMBER")
	private Integer citizenNumber;
}
