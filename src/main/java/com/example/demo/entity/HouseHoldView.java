package com.example.demo.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@Entity
@Table(name = "HOUSEHOLD_VIEW")
public class HouseHoldView extends BaseEntity{

	@Id
    @Column(name = "HOUSEHOLD_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name = "HOUSEHOLD_NUMBER")
	private String houseHoldNumber;
	
	@Column(name = "ADDRESS")
	private String address;
	
	@Column(name = "GROUP_ADDRESS")
	private String groupAddress;
	
	@OneToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GROUP_ADDRESS", insertable = false, updatable = false)
	private ReferenceData groupAddressRef;
	
	@Column(name = "CITIZEN_ID")
	private Long citizenId;
	
//	@OneToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "CITIZEN_ID", insertable = false, updatable = false)
//	private Citizen citizen;
	
	@Column(name = "STATUS")
	private Boolean status;
	
    @Column(name = "HOST_NAME")
    private String hostName;
    
    @Column(name = "NUMBER_CITIZEN")
    private Integer citizenNumber;
    
    @Column(name = "MEMBER_NAME")
    private String memberName;
}

