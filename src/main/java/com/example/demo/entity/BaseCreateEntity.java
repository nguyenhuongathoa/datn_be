package com.example.demo.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Setter
@Getter
@EntityListeners(AuditingEntityListener.class)
public class BaseCreateEntity implements IEntityId {
	
	@Column(name = "CREATE_DATE",columnDefinition =  "TIMESTAMP", insertable = true, updatable = true)
	@CreatedDate
	private LocalDateTime createDate;
	
	@Column(name ="Create_by", insertable = true, updatable = true)
	private String createBy;
	
	public BaseCreateEntity create(String createBy) {
		this.setId(null);
		this.createBy = createBy;
		this.createDate = LocalDateTime.now();
		return this;
		
	}
    public BaseCreateEntity autoCreateOrUpdate(String modifiedBy) {
    	if(this.getId() == null|| this.getId() == 0) {
    		create(modifiedBy);
    	}
    	return this;
		
	}
	
	@Override
	public Long getId() {
		return null;
	}
	
	@Override
	public void setId(Long od) {
		
	}

}
