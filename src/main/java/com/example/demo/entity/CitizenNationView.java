package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@Entity
@Table(name = "CITIZEN_NATION_VIEW")
public class CitizenNationView {
	@Id
	@Column(name = "NAME")
	private String Name;
	

	@Column(name = "CITIZEN_NUMBER")
	private Integer citizenNumber;
}
