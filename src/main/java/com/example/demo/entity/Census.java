package com.example.demo.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.LastModifiedDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@Entity
@Table(name = "CENSUS")
public class Census {
	@Id
    @Column(name = "CENSUS_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long censusId;
	
	@Column(name = "TOTAL_CITIZEN")
	private Integer totalCitizen;
	
	@Column(name = "MALE_NUMBER")
	private Integer maleNumber;
	
	@Column(name = "FEMALE_NUMBER")
	private Integer femaleNumber;
	
	@Column(name = "YOUTH_NUMBER")
	private Integer youthNumber;
	
	@Column(name = "MIDDLE_NUMBER")
	private Integer middleNumber;
	
	@Column(name = "OLD_NUMBER")
	private Integer oldNumber;
	
	@Column(name = "CREATE_BY")
	private String createBy;
	
	@Column(name = "CREATE_DATE", columnDefinition = "TIMESTAMP")
	@LastModifiedDate
	private LocalDateTime createDate;

}
