package com.example.demo.dto;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.example.demo.common.DataUtils;
import com.example.demo.entity.HouseHold;
import com.example.demo.entity.HouseHoldCitizen;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HouseHoldDto {

	private Long id;

	private String houseHoleNumber;

	private String address;

	private String groupAddress;
	
	private String groupAddressName;

	private Long citizenId;
	
	private String hostName;

	private Boolean status;
	
	private Integer citizenNumber;

	private String createBy;

	private LocalDateTime createDate;

	private String updateBy;

	private LocalDateTime updateDate;
	
	
	
}
