package com.example.demo.dto;

import java.time.LocalDateTime;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CitizenDto {

    private Long id;
	
	private String fullName;
	
	private LocalDateTime dateOfBirth;
	
	private String sex;
	
	private String indentificationType;
	
	private String indentificationNumber;

	private String passport;
	
	private String phoneNumber;
	
	private String email;
	
	private String nation;
	
	private String religion;
	
	private String countyName;
	
	private String regionBirth;
	
	private String residentAddress;
	
	private String temporaryResidenceAddress;
	
	private String education;
	
	private String educationGraduate;
	
	private String speciallized;
	
	private String job;
	
	private Boolean statusAlive;
	
	private String description;
	
	private Integer vaccination;
	
	private String createBy;
	
	private LocalDateTime createDate;
	
	private String updateBy;
	
	private LocalDateTime updateDate;
}
