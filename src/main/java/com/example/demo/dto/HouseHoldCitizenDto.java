package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HouseHoldCitizenDto {

	private Long id;
	
	private Long citizenId;
    
	private Long houseHoldId;
    
    private String relationShipHouseHold;
    
    //trạng thái dang ở dạng nào
    private String statusType;
    
    //còn ở đấy hay không
    private Boolean isactive;
}
