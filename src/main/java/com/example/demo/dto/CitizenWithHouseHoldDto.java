package com.example.demo.dto;

import java.time.LocalDateTime;

import javax.persistence.Column;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CitizenWithHouseHoldDto {

	     private Long houseHoldId;
		
		private String relationshipHouseHold;
		
	    private Boolean belongHouseHold;
	    
	    //trạng thái dang ở dạng nào
	    private String statusType;
	    
	    //còn ở đấy hay không
	    private Boolean isactive;
		
		private CitizenDto citizen;
		
}
