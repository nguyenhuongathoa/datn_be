package com.example.demo.dto.request;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.example.demo.dto.HouseHoldCitizenDto;
import com.example.demo.entity.Citizen;
import com.example.demo.entity.ReferenceData;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HouseHoldRequestDto {
	
    private Long id;
	
	
	private String houseHoldNumber;
	
	private String address;
	
	private String groupAddress;
	
	private Long citizenId;
	
	private Citizen citizen;
	
	private Boolean status;
	
	private String createBy;

	private LocalDateTime createDate;

	private String updateBy;

	private LocalDateTime updateDate;
	
	private List<HouseHoldCitizenDto> listcitizen;
}
