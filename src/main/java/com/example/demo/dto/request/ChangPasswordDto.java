package com.example.demo.dto.request;

import java.time.LocalDateTime;
import java.util.List;

import com.example.demo.dto.HouseHoldCitizenDto;
import com.example.demo.entity.Citizen;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChangPasswordDto {
	
	private Long id;

	private Long citizenId;

	private String userName;

	private String userPassword;

	private String userPasswordNew;
	
	private String userPasswordConfirm;

}
