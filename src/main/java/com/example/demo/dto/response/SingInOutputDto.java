package com.example.demo.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SingInOutputDto {

	private Long userId;
	private Long citizenId;
	private String userName;
	private String fullName;
	private String role;
	private String roleType;
}
