package com.example.demo.dto.response;

import java.util.List;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PagingDto {

	private int maxCount;
	private int pageIndex;
	private int pageSize;
	private int listSize;
	
	private Object object;
	
	public PagingDto(int maxCount, int pageIndex, int pageSize) {
		this.maxCount = maxCount;
		this.pageIndex = pageIndex;
		this.pageSize = pageSize;
	}
}
