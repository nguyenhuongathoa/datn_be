package com.example.demo.dto.response;

import java.util.List;

import com.example.demo.dto.request.SinginInputDto;
import com.example.demo.entity.HouseHold;
import com.example.demo.entity.HouseHoldCitizen;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HouseHoldCreatRespon {

	private HouseHold household;
	private List<HouseHoldCitizen> listHouseHoldCitizen;
}
