package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.common.DataUtils;
import com.example.demo.dto.UsersDto;
import com.example.demo.dto.request.ChangPasswordDto;
import com.example.demo.dto.request.SinginInputDto;
import com.example.demo.dto.response.PagingDto;
import com.example.demo.dto.response.SingInOutputDto;
import com.example.demo.entity.CitizenHouseView;
import com.example.demo.entity.Notice;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import static com.example.demo.common.AssertUtils.assertFalse;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	// private static final String JWT_HEADER =
	// "{\"alg\":\"HS256\",\"typ\":\"JWT\"}";

	public SingInOutputDto singIn(SinginInputDto dto) {
		User user = userRepository.checkSingIn(dto.getUserName(), dto.getPassWord());
		if (DataUtils.nullObject(user)) {
			assertFalse(true, "20000", "Tên đăng nhập hoặc mật khẩu không hợp lệ!");
		} else {
			SingInOutputDto respon = SingInOutputDto.builder().citizenId(user.getCitizenId()).userId(user.getId())
					.userName(user.getUserName())
					.role(user.getRole())
					.roleType(user.getRoleRef().getName())
					.fullName(user.getCitizen().getFullName()).build();
			return respon;
		}
		return null;
	}

	public User createUserNomal(UsersDto user) {
		User checkUser = userRepository.checkUserExit(user.getUserName(), user.getCitizenId());
		if (DataUtils.notnullObject(checkUser)) {
			assertFalse(true, "20000", "Tên đăng nhập đã tồn tại hoặc người dùng này đã có tài khoản");
		}else {
			User useEntity = new User().builder().userName(user.getUserName()).userPassword(user.getUserPassword())
					.role(user.getRole()).citizenId(user.getCitizenId()).build();
			useEntity = userRepository.save(useEntity);
			return useEntity;
		}
		return null;
	}
	public Boolean deleteUser(Long id) {
		User user = userRepository.getById(id);
		if (DataUtils.nullObject(user)) {
			assertFalse(true, "20000", "Tài khoản không tồn tại");
		}else{
			userRepository.delete(user);
		}
		return true;
	}
	//update role and update password
	public User updateUser(User user) {
		User checkUser = userRepository.getByCitizenId(user.getCitizenId());
		if (DataUtils.nullObject(checkUser)) {
			assertFalse(true, "20000", "Tài khoản không tồn tại");
		}else{
			checkUser.setUserPassword(user.getUserPassword());
			checkUser.setRole(user.getRole());
			userRepository.save(user);
		}
		return user;
	}

	// update password
	public User updatePasswordUser(ChangPasswordDto user) {
		User checkUser = userRepository.getByCitizenId(user.getCitizenId());
		assertFalse(DataUtils.nullObject(checkUser), "20000", "Tài khoản không tồn tại!");
		assertFalse(!checkUser.getUserPassword().equals(user.getUserPassword()), "20001", "Mật khẩu cũ không đúng!");
		assertFalse(!user.getUserPasswordConfirm().equals(user.getUserPasswordNew()), "20001", "Mật khẩu xác nhận lại không trùng nhau!");
		checkUser.setUserPassword(user.getUserPasswordNew());
		userRepository.save(checkUser);

		return checkUser;
	}

	public PagingDto getListUsers() {	
		List<User> user = userRepository.getListCitizen();
	    if(user != null) {
	    	PagingDto result = new PagingDto(user.size(), 1, user.size());	
				result.setListSize(user.size());
				result.setObject(user);
				return result;
			}
		return null;
		
	}
}
