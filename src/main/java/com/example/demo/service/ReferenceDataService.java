package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.ReferenceData;
import com.example.demo.repository.ReferenceDataRepository;

@Service
public class ReferenceDataService {

	@Autowired
	private ReferenceDataRepository referenceDataRepository;
	
	public List<ReferenceData> getRefFromType(String type){
		List<ReferenceData> listRef = referenceDataRepository.getReferenceFromType(type);
		return listRef;
	}
}
