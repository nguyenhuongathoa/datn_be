package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.response.CharPieDto;
import com.example.demo.entity.CitizenAgeView;
import com.example.demo.entity.CitizenEducationView;
import com.example.demo.entity.CitizenNationView;
import com.example.demo.entity.CitizenReligionView;
import com.example.demo.entity.CitizenSexView;
import com.example.demo.entity.CitizenVaccinView;
import com.example.demo.repository.CitizenAgeViewRepository;
import com.example.demo.repository.CitizenEducationViewRepository;
import com.example.demo.repository.CitizenNationViewRepository;
import com.example.demo.repository.CitizenReligionViewRepository;
import com.example.demo.repository.CitizenSexViewRepository;
import com.example.demo.repository.CitizenVaccinViewRepository;

@Service
public class TotalService {
	
	@Autowired
	private CitizenSexViewRepository citizenSexViewRepo;
	
	@Autowired
	private CitizenVaccinViewRepository citizenVaccinViewRepository;
	
	@Autowired
	private CitizenAgeViewRepository citizenAgeViewRepository;
	
	@Autowired
	private CitizenNationViewRepository citizenNationViewRepository;
	
	@Autowired
	private CitizenEducationViewRepository citizenEducationViewRepository;
	
	@Autowired
	private CitizenReligionViewRepository citizenReligionViewRepository;
	
	public CharPieDto getTotalSex() {
		List<CitizenSexView>  result = citizenSexViewRepo.findAll();
		List<Integer> seri = new ArrayList<Integer>();
		List<String> lable = new ArrayList<String>();
		CharPieDto charPieDto = new CharPieDto();
		for(int i = 0; i< result.size(); i++) {
			seri.add(result.get(i).getCitizenNumber());
			lable.add(result.get(i).getName());
		}
		charPieDto.setLabels(lable);
		charPieDto.setSeries(seri);
		return charPieDto;
	}
	
	public CharPieDto getTotalVaccin() {
		List<CitizenVaccinView>  result = citizenVaccinViewRepository.findAll();
		List<Integer> seri = new ArrayList<Integer>();
		List<String> lable = new ArrayList<String>();
		CharPieDto charPieDto = new CharPieDto();
		for(int i = 0; i< result.size(); i++) {
			seri.add(result.get(i).getCitizenNumber());
			lable.add(result.get(i).getVaccination()+ " Mũi");
		}
		charPieDto.setLabels(lable);
		charPieDto.setSeries(seri);
		return charPieDto;
	}
	
	public CharPieDto getTotalAge() {
		List<CitizenAgeView>  result = citizenAgeViewRepository.findAll();
		List<Integer> seri = new ArrayList<Integer>();
		List<String> lable = new ArrayList<String>();
		CharPieDto charPieDto = new CharPieDto();
		for(int i = 0; i< result.size(); i++) {
			seri.add(result.get(i).getCitizenNumber());
			if(result.get(i).getAgeGroup()==1) {
				lable.add("Từ 0-15 tuổi");
			}else if(result.get(i).getAgeGroup()==2) {
				lable.add("Từ 16- 65 tuổi");
			}else {
				lable.add(">65 tuổi");
			}
			
		}
		charPieDto.setLabels(lable);
		charPieDto.setSeries(seri);
		return charPieDto;
	}
	public CharPieDto getTotalNation() {
		List<CitizenNationView>  result = citizenNationViewRepository.findAll();
		List<Integer> seri = new ArrayList<Integer>();
		List<String> lable = new ArrayList<String>();
		CharPieDto charPieDto = new CharPieDto();
		int dtkhac = 0;
		for(int i = 0; i< result.size(); i++) {
			if("KINH".equals(result.get(i).getName())){
				seri.add(result.get(i).getCitizenNumber());
				lable.add("Dân tộc Kinh");
			}else {
				dtkhac = dtkhac + result.get(i).getCitizenNumber();
			}
		} 
		lable.add("Dân tộc khác");
		seri.add(dtkhac);
		charPieDto.setLabels(lable);
		charPieDto.setSeries(seri);
		charPieDto.setListObject(result);
		return charPieDto;
	}
	
	public CharPieDto getTotalEducation() {
		List<CitizenEducationView> result = citizenEducationViewRepository.findAll();
		List<Integer> seri = new ArrayList<Integer>();
		List<String> lable = new ArrayList<String>();
		CharPieDto charPieDto = new CharPieDto();
		for (int i = 0; i < result.size(); i++) {
			lable.add(result.get(i).getName());
			seri.add(result.get(i).getCitizenNumber());

		}
		charPieDto.setLabels(lable);
		charPieDto.setSeries(seri);
		charPieDto.setListObject(result);
		return charPieDto;
	}
	
	
	public CharPieDto getTotalReligion() {
		List<CitizenReligionView>  result = citizenReligionViewRepository.findAll();
		List<Integer> seri = new ArrayList<Integer>();
		List<String> lable = new ArrayList<String>();
		CharPieDto charPieDto = new CharPieDto();
		int dtkhac = 0;
		for(int i = 0; i< result.size(); i++) {
			if("KHÔNG".equals(result.get(i).getName())){
				seri.add(result.get(i).getCitizenNumber());
				lable.add("Không");
			}else {
				dtkhac = dtkhac + result.get(i).getCitizenNumber();
			}
		} 
		lable.add("Có");
		seri.add(dtkhac);
		charPieDto.setLabels(lable);
		charPieDto.setSeries(seri);
		charPieDto.setListObject(result);
		return charPieDto;
	}

}
