package com.example.demo.service;

import static com.example.demo.common.AssertUtils.assertFalse;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityListeners;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.mapping.Array;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.jaxb.SpringDataJaxb.PageDto;
import org.springframework.stereotype.Service;

import com.example.demo.common.DataUtils;
import com.example.demo.dto.NoticeDto;
import com.example.demo.dto.response.PagingDto;
import com.example.demo.entity.Notice;
import com.example.demo.mapper.NoticeMapper;
import com.example.demo.repository.NoticeRepository;
import com.example.demo.repository.NoticeSearchRepo;

import lombok.RequiredArgsConstructor;

@Service
public class NoticeService {


    @Autowired
	private NoticeRepository noticeRepository;
    
     
    @Autowired 
    private NoticeSearchRepo NoticeSearchRepo;

	
	public PagingDto getAllNotice(int page, int size, String type, String searchValue, String status, String fromDate, String toDate){
		int count  = NoticeSearchRepo.countTotalNotice(type, searchValue,status, fromDate, toDate);
		PagingDto result = new PagingDto(count,page, size);
		if(count>0) {
			int rowBegin = (page - 1)*size + 1;
			int rowEnd = page*size;
			List<Notice> listNotice= NoticeSearchRepo.search(rowBegin, rowEnd, type, searchValue,status, fromDate, toDate);
			if(listNotice != null && listNotice.size()!= 0) {
				result.setListSize(listNotice.size());
				result.setObject(listNotice);
				//listNotice.forEach(listNotice.se);
			}
		}else {
			List<Notice> li = new ArrayList<Notice>();
			result.setObject(li);
		}
		return result;		
	}
	public Notice getNoticeFromId(Long id) {
		Notice notice = noticeRepository.getById(id);
		return notice;
	}
	
	public Notice CreateNotice(NoticeDto notice) {
		if (notice.getStatus().equals("PENDING")) {
			assertFalse(
					DataUtils.nullOrEmpty(notice.getNoticecontent()) || DataUtils.nullOrEmpty(notice.getNoticeCategory())
							|| DataUtils.nullOrEmpty(notice.getSubject()),
					"10001", "Phải có đầy đủ thông tin trước khi phê duyệt");
		}
		Notice noticeEntity = Notice.builder().subject(notice.getSubject()).noticeCategory(notice.getNoticeCategory())
				.noticecontent(notice.getNoticecontent()).status(notice.getStatus()).build();
		noticeEntity.autoCreateOrUpdate(notice.getCreateBy());
		noticeEntity.setCreateDate(LocalDateTime.now());
		noticeRepository.save(noticeEntity);
		return noticeEntity;
	}
	public Notice updateNotice(Notice notice) {
		Notice test = noticeRepository.getById(notice.getId());
		if(test == null) {
			assertFalse(true,"10000","Notice không tồn tại!");
		}
		if (notice.getStatus().equals("PENDING")) {
			assertFalse(
					DataUtils.nullOrEmpty(notice.getNoticecontent()) || DataUtils.nullOrEmpty(notice.getNoticeCategory())
							|| DataUtils.nullOrEmpty(notice.getSubject()),
					"10001", "Phải có đầy đủ thông tin trước khi phê duyệt");
		}
		notice.setUpdateDate(LocalDateTime.now());
		//notice.setUpdateBy(updateBy);
		noticeRepository.save(notice);
		return notice;
	}
	public boolean deleteNotice(Long id) {
		Notice test = noticeRepository.getById(id);
		if(test == null) {
			assertFalse(true,"10000","Notice không tồn tại!");
		}
		noticeRepository.delete(test);
		return true;
	}
	
}
