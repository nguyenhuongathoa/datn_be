package com.example.demo.service;

import static com.example.demo.common.AssertUtils.assertFalse;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.xml.ws.Holder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.jaxb.SpringDataJaxb.PageDto;
import org.springframework.stereotype.Service;

import com.example.demo.common.DataUtils;
import com.example.demo.dto.HouseHoldDto;
import com.example.demo.dto.request.HouseHoldRequestDto;
import com.example.demo.dto.response.HouseHoldCreatRespon;
import com.example.demo.dto.response.PagingDto;
import com.example.demo.entity.Citizen;
import com.example.demo.entity.HouseHold;
import com.example.demo.entity.HouseHoldCitizen;
import com.example.demo.entity.HouseHoldView;
import com.example.demo.entity.Notice;
import com.example.demo.repository.CitizenRepository;
import com.example.demo.repository.HouseHoldCitizenRepository;
import com.example.demo.repository.HouseHoldRepo;
import com.example.demo.repository.HouseHoldRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class HouseHoldService {

	@Autowired
	private HouseHoldRepository houseHoldRepository;
	
	@Autowired
	private HouseHoldRepo houseHoldRepo;
	
	@Autowired
	private CitizenRepository citizenRepository;
	
	@Autowired
	private HouseHoldCitizenRepository houseHoldCitizenRepository;
	
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
    private HouseHoldCitizenService houseHoldCitizenService;
	
	
	public PagingDto getAllHouseHold(Integer page,Integer size, String searchValue, Boolean status,String citizenNumber) {
		int count  = houseHoldRepo.countTotalHouseHold(searchValue,status,citizenNumber);
		PagingDto result = new PagingDto(count,page, size);
		if(count>0) {
			int rowBegin = (page - 1)*size + 1;
			int rowEnd = page*size;
			List<HouseHoldView> listHouseHold= houseHoldRepo.search(rowBegin, rowEnd, searchValue,status,citizenNumber);
			result.setObject(listHouseHold);
//			if(listHouseHold != null && listHouseHold.size()!= 0) {
//				result.setListSize(listHouseHold.size());
//				List<HouseHoldDto> listDto = this.getFromListHouseHold(listHouseHold);
//				result.setObject(listHouseHold);
//				//listNotice.forEach(listNotice.se);
//			}
		}else {
			List<HouseHold> li = new ArrayList<HouseHold>();
			result.setObject(li);
		}
		return result;
	}
	public HouseHoldCreatRespon createHouseHold(HouseHoldRequestDto dto) {
		assertFalse(DataUtils.nullObject(dto), "40000",
				"Cần điền đủ thông tin bắt buộc");		
		//check citizen đó đã là chủ hộ của hộ nào đang tồn tại không
		HouseHold check2 = houseHoldRepository.getByCitizenId(dto.getCitizenId());
		assertFalse(DataUtils.notnullObject(check2), "40002",
				"Một người không thể làm chủ hộ của 2 hộ gia đình.");
		//check citizen đó có đang thuộc hộ khẩu của hộ nào không     --->cần trừ trường hợp thuộc hộ khẩu hiện tại
		HouseHoldCitizen check3 = houseHoldCitizenRepository.getByCitizenId(dto.getCitizenId());
		assertFalse(DataUtils.notnullObject(check3), "40003",
				"Một người không thể thuộc 2 hộ khẩu");
		this.checkHouseHoldCitizenId(dto);
		HouseHold enti = new HouseHold().builder()
				.address(dto.getAddress())
				.groupAddress(dto.getGroupAddress())
				.citizenId(dto.getCitizenId())
				.status(true)
				.houseHoldNumber(dto.getHouseHoldNumber())
				.build();
		enti.autoCreateOrUpdate(dto.getCreateBy());
		enti.setCreateDate(LocalDateTime.now());
		houseHoldRepository.save(enti);
		HouseHoldCreatRespon result = new HouseHoldCreatRespon();
		result.setHousehold(enti);
		if(dto.getListcitizen()!= null && dto.getListcitizen().size()!=0) {
			List<HouseHoldCitizen> listHouseHold =	houseHoldCitizenService.createHouseholdCitizen(dto.getListcitizen(), enti.getId());
			result.setListHouseHoldCitizen(listHouseHold);
		}
		return result;
	}
	private void checkHouseHoldCitizenId(HouseHoldRequestDto dto) {
		//check có tồn tại citizen đấy không
				Citizen check1 = citizenRepository.getById(dto.getCitizenId());
				assertFalse(DataUtils.nullObject(check1), "40001",
						"Công dân làm chủ nhà đang không tồn tại trong hệ thông.");
				
				
				
	}
	public HouseHoldCreatRespon updateHouseHold(HouseHoldRequestDto dto) {
		HouseHoldCreatRespon result = new HouseHoldCreatRespon();
		HouseHold test = houseHoldRepository.getById(dto.getId());
		if(test == null) {
			assertFalse(true,"10000","Hộ dân không tồn tại!");
		}
		HouseHold houseHold = objectMapper.convertValue(dto, HouseHold.class);
		this.checkHouseHoldCitizenId(dto);
		houseHold.setUpdateDate(LocalDateTime.now());
		houseHold = houseHoldRepository.save(houseHold);
		result.setHousehold(houseHold);
		if(dto.getListcitizen()!= null && dto.getListcitizen().size()!=0) {
			List<HouseHoldCitizen> listHouseHold =	houseHoldCitizenService.updateHouseholdCitizen(dto.getListcitizen(), houseHold.getId());
			result.setListHouseHoldCitizen(listHouseHold);
		}
		return result;
	}
	@Transactional
	public Boolean deleteHouseHold(Long id) {
		HouseHold test = houseHoldRepository.getById(id);
		if(test == null) {
			assertFalse(true,"10000","Hộ dân không tồn tại!");
		}
		houseHoldCitizenRepository.deleteByHouseHoldId(id);
		houseHoldRepository.delete(test);
		return true;
	}
	
	public HouseHold getHouseHoldFromID(Long id) {
		HouseHold houseHold = houseHoldRepository.getById(id);
		if(houseHold == null) {
			assertFalse(true,"10000","Hộ dân không tồn tại!");
		}
		return houseHold;
	}
	private List<HouseHoldDto> getFromListHouseHold(List<HouseHold> entities){
		if(DataUtils.nullObject(entities)) {
			return null;
		}else {
			List<HouseHoldDto> listDto = new ArrayList<HouseHoldDto>();
			for(HouseHold entity : entities) {
				listDto.add(this.getfromHouseHold(entity));
			}
			return listDto;
		}
	}
	private HouseHoldDto getfromHouseHold(HouseHold entity) {
		if(DataUtils.nullObject(entity)) {
			return null;
		}else {
			List<HouseHoldCitizen> listHC = houseHoldCitizenRepository.getByHouseHold(entity.getId());
			HouseHoldDto dto = new HouseHoldDto().builder()
					.id(entity.getId())
					.houseHoleNumber(entity.getHouseHoldNumber())
					.address(entity.getAddress())
					.groupAddress(entity.getGroupAddress())
					.groupAddressName(entity.getGroupAddressRef().getName())
					.citizenId(entity.getCitizenId())
					.hostName(entity.getCitizen().getFullName())
					.status(entity.getStatus())
					.citizenNumber(DataUtils.nullObject(listHC) ? 1 : listHC.size()+1)
					.updateBy(entity.getUpdateBy())
					.updateDate(entity.getUpdateDate())
					.createBy(entity.getCreateBy())
					.createDate(entity.getCreateDate())
					.build();
			return dto;
		}
	}
}
