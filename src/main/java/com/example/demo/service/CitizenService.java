package com.example.demo.service;

import static com.example.demo.common.AssertUtils.assertFalse;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.unit.DataUnit;

import com.example.demo.common.DataUtils;
import com.example.demo.dto.CitizenDto;
import com.example.demo.dto.CitizenWithHouseHoldDto;
import com.example.demo.dto.response.PagingDto;
import com.example.demo.entity.Citizen;
import com.example.demo.entity.CitizenHouseView;
import com.example.demo.entity.HouseHold;
import com.example.demo.entity.HouseHoldCitizen;
import com.example.demo.entity.Notice;
import com.example.demo.entity.User;
import com.example.demo.repository.CitizenRepo;
import com.example.demo.repository.CitizenRepository;
import com.example.demo.repository.HouseHoldCitizenRepository;
import com.example.demo.repository.HouseHoldRepository;
import com.example.demo.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Setter;

@Service
public class CitizenService {

	@Autowired
	private CitizenRepository citizenRepository;

	@Autowired
	private HouseHoldCitizenRepository houseHoldCitizenRepository;

	@Autowired
	private HouseHoldRepository houseHoldRepository;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private CitizenRepo citizenRepo;
	
	@Autowired
	private UserRepository userRepository;

	public PagingDto getAllCitizens(int page, int size, String searchValue, Boolean status, String sex,
			String indentification, String religion, String nation, String education, String educationGraduate,
			String speciallized,  String vaccination, int ageTo, int ageFrom) {
		int count = citizenRepo.countTotalCitizen(searchValue, status, sex, indentification, religion, nation,
				education, educationGraduate, speciallized,  vaccination, ageTo, ageFrom);
		PagingDto result = new PagingDto(count, page, size);
		if (count > 0) {
			int rowBegin = (page - 1) * size + 1;
			int rowEnd = page * size;
			List<CitizenHouseView> listCitizen = citizenRepo.search(rowBegin, rowEnd, searchValue, status, sex,
					indentification, religion, nation, education, educationGraduate, speciallized, vaccination,
					ageTo, ageFrom);
			if (listCitizen != null && listCitizen.size() != 0) {
				result.setListSize(listCitizen.size());
				result.setObject(listCitizen);

			}
		} else {
			List<Notice> li = new ArrayList<Notice>();
			result.setObject(li);
		}
		return result;
	}

	public Citizen createCitizen(CitizenDto citizen) {
		this.validateCreate(citizen);
		Citizen cizen = objectMapper.convertValue(citizen, Citizen.class);
		cizen.setId(null);
		if(citizen.getVaccination() == null) {
			cizen.setVaccination(0);
		}
		cizen.autoCreateOrUpdate(citizen.getCreateBy());
		cizen = citizenRepository.save(cizen);
		return cizen;
	}

	@Transactional
	public Citizen createCitizenWithHouse(CitizenWithHouseHoldDto dto) {
		assertFalse(DataUtils.nullObject(dto), "30000", "Bạn phải nhập đủ các trường bắt buộc");
		assertFalse(DataUtils.nullOrEmpty(dto.getRelationshipHouseHold()), "30010",
				"Bạn phải nhập trường quan hệ với chủ nhà");
		HouseHold house = houseHoldRepository.getById(dto.getHouseHoldId());
		assertFalse(DataUtils.nullObject(house), "30011", "Hộ gia đình không tồn tại");
		this.validateCreate(dto.getCitizen());
		Citizen citizen = objectMapper.convertValue(dto.getCitizen(), Citizen.class);
		citizen.setId(null);
		citizen.autoCreateOrUpdate(dto.getCitizen().getCreateBy());
		citizen = citizenRepository.save(citizen);
		HouseHoldCitizen houseCitizen = new HouseHoldCitizen().builder().houseHoldId(dto.getHouseHoldId())
				.relationShipHouseHold(dto.getRelationshipHouseHold()).citizenId(citizen.getId())
				.isactive(dto.getIsactive()).statusType(dto.getStatusType()).build();
		houseHoldCitizenRepository.save(houseCitizen);
		return citizen;
	}

	public Citizen updateCitizen(CitizenDto dto) {
		this.validate(dto);
		Citizen citizen = objectMapper.convertValue(dto, Citizen.class);
		this.validateUpdate(dto);
		citizen.setUpdateDate(LocalDateTime.now());
		if(citizen.getVaccination() == null) {
			citizen.setVaccination(0);
		}
		citizenRepository.save(citizen);
		return citizen;
	}

	public Citizen getCitizenById(Long id) {
		Citizen citizen = citizenRepository.getById(id);
		assertFalse(DataUtils.nullObject(citizen), "30009", "Công dân này chưa tồn tại trong hệ thống");
		return citizen;
	}

	public Boolean deleteCitizen(Long id) {
		User user = userRepository.getByCitizenId(id);
		assertFalse(DataUtils.notnullObject(user), "30010",
				"Công dân đang được phân quyền ở phường, nếu bạn muốn xóa phải xóa tài khoản của người này khỏi hệ thống trước!");
		Citizen citizen = citizenRepository.getById(id);
		assertFalse(DataUtils.nullObject(citizen), "30009", "Công dân này chưa tồn tại trong hệ thống");
		HouseHoldCitizen hhc = houseHoldCitizenRepository.getByCitizenIdAlone(id);
		HouseHold hh = houseHoldRepository.getByCitizenId(id);
		assertFalse(DataUtils.notnullObject(hhc)||DataUtils.notnullObject(hh), "30011",
				"Công dân đang là thành viên của hộ gia đình, nếu bạn muốn xóa phải xóa tài khoản của người này khỏi hộ gia đình trước!");
		citizenRepository.delete(citizen);
		return true;
	}

	private void validateCreate(CitizenDto citizend) {
		assertFalse(DataUtils.nullObject(citizend), "30000", "Bạn cần nhập các trường bắt buộc!");
		Citizen check = citizenRepository.getByIndentification(citizend.getIndentificationType(),
				citizend.getIndentificationNumber());
		assertFalse(DataUtils.notnullObject(check), "30001",
				"Công dân này đã tồn tại trong hệ thống! Bạn nên kiểm tra lại theo số định danh.");
		this.validate(citizend);

	}

	private void validateUpdate(CitizenDto citizen) {
		assertFalse(DataUtils.nullObject(citizen), "30000", "Bạn cần nhập các trường bắt buộc!");
		Citizen check = citizenRepository.getById(citizen.getId());
		assertFalse(DataUtils.nullObject(check), "30009", "Công dân này chưa tồn tại trong hệ thống");
		this.validate(citizen);
	}

	private void validate(CitizenDto citizend) {
		assertFalse(DataUtils.nullOrEmpty(citizend.getFullName()), "30002", "Bạn phải nhập đủ họ và tên công dân.");
		assertFalse(DataUtils.nullOrEmpty(citizend.getSex()), "30003", "Bạn phải nhập đủ họ và tên công dân.");
//		assertFalse(DataUtils.nullOrEmpty(citizend.getIndentificationType()), "30004",
//				"Bạn phải chọn loại giấy tờ định danh.");
//		assertFalse(DataUtils.nullOrEmpty(citizend.getIndentificationNumber()), "30005", "Bạn phải nhập số CMDN/CCCD.");
		assertFalse(citizend.getDateOfBirth() == null, "30006", "Bạn phải nhập ngày tháng năm sinh.");
		assertFalse(DataUtils.nullOrEmpty(citizend.getCountyName()), "30007", "Bạn phải nhập trường Quốc Tịch. ");
		assertFalse(citizend.getStatusAlive() == null, "30008", "Bạn phải chọn trường trạng thái tồn tại ");
	}

	public List<Citizen> getCitizenForHost(Long houseHoldId) {
		if(houseHoldId ==null || houseHoldId == 0) {
			List<Long> listhostId = houseHoldRepository.getlistHostWithNotId();
			List<Long> listCitizen = houseHoldCitizenRepository.getListForHostnotID();
			listhostId.addAll(listCitizen);
			if(listhostId == null||listhostId.size()==0) {
				return citizenRepository.findAll();
			}
			return  citizenRepository.getListCitizenForAssign(listhostId);
		}else {
			List<Long> listhostId = houseHoldRepository.getlistHost(houseHoldId);
			List<Long> listCitizen = houseHoldCitizenRepository.getListForHost(houseHoldId);
			listhostId.addAll(listCitizen);
			if(listhostId == null||listhostId.size()==0) {
				return citizenRepository.findAll();
			}
			return  citizenRepository.getListCitizenForAssign(listhostId);
		}
	}
	public List<Citizen> getCitizenForUser() {
		List<Long> listUser = userRepository.getlistCitizenId();
		List<Citizen> listHost = citizenRepository.getListCitizenForAssign(listUser);
		return listHost;
	}

}
