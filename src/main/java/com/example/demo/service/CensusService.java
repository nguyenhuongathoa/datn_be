package com.example.demo.service;

import static com.example.demo.common.AssertUtils.assertFalse;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Census;
import com.example.demo.entity.Citizen;
import com.example.demo.entity.CitizenAgeView;
import com.example.demo.entity.CitizenSexView;
import com.example.demo.repository.CensusRepository;
import com.example.demo.repository.CitizenAgeViewRepository;
import com.example.demo.repository.CitizenRepository;
import com.example.demo.repository.CitizenSexViewRepository;

@Service
public class CensusService {

	@Autowired
	private CensusRepository censusRepository;
	
	@Autowired
	private CitizenSexViewRepository citizenSexViewRepository;
	
	@Autowired
	private CitizenAgeViewRepository citizenAgeViewRepository;
	
	@Autowired
	private CitizenRepository citizenRepository;
	
	public List<Census> getAllCensus(){
		List<Census>  result= censusRepository.getAll();
		return result;
	}

	public Census saveCensus(Census entity) {
		if (entity == null || entity.getTotalCitizen() == null || entity.getMaleNumber() == null
				|| entity.getFemaleNumber() == null || entity.getYouthNumber() == null||entity.getMiddleNumber()== null||entity.getOldNumber()== null) {
			assertFalse(true, "30000", "Bạn phải nhập đủ các trường bắt buộc");
		}
		entity.setCreateDate(LocalDateTime.now());
			censusRepository.save(entity);
		return entity;
	}
	public Boolean deleteCensus(Long censusID) {
		Census census = censusRepository.getCensusById(censusID);
		assertFalse(census == null, "30000", "Thống kê không tồn tại");
		censusRepository.deleteById(censusID);
		return true;
	}
	
	public Census Caculate() {
		Census result = new Census();
		List<Citizen> total = citizenRepository.getListCitizenAlive();
		result.setTotalCitizen(total.size());
		List<CitizenSexView>  sex = citizenSexViewRepository.findAll();
		List<CitizenAgeView>  age = citizenAgeViewRepository.findAll();
		for(int i = 0; i<sex.size();i++) {
			if(sex.get(i).getName().equals("Nam")) {
				result.setMaleNumber(sex.get(i).getCitizenNumber());
			}else {
				result.setFemaleNumber(sex.get(i).getCitizenNumber());
			}
		}
		for(int i = 0; i< age.size(); i++) {		
			if(age.get(i).getAgeGroup()==1) {
				result.setYouthNumber(age.get(i).getCitizenNumber());
			}else if(age.get(i).getAgeGroup()==2) {
				result.setMiddleNumber(age.get(i).getCitizenNumber());
			}else {
				result.setOldNumber(age.get(i).getCitizenNumber());
			}
			
		}
		return result;
	}
}
