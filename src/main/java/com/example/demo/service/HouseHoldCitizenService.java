package com.example.demo.service;

import static com.example.demo.common.AssertUtils.assertFalse;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.common.DataUtils;
import com.example.demo.dto.HouseHoldCitizenDto;
import com.example.demo.entity.HouseHold;
import com.example.demo.entity.HouseHoldCitizen;
import com.example.demo.repository.HouseHoldCitizenRepository;
import com.example.demo.repository.HouseHoldRepository;

@Service
public class HouseHoldCitizenService {

	@Autowired
	private HouseHoldCitizenRepository householdCitizenRepo;
	
	@Autowired
	private HouseHoldRepository houseHoldRepo;
	
	@Transactional
	public List<HouseHoldCitizen> createHouseholdCitizen(List<HouseHoldCitizenDto> dto, Long houseHoldId) {
		List<HouseHoldCitizen> houseHoldCitizen = validateHouseCitizen(dto, houseHoldId);
		houseHoldCitizen = householdCitizenRepo.saveAll(houseHoldCitizen);
		return houseHoldCitizen;
	}
	
	@Transactional
	public List<HouseHoldCitizen> updateHouseholdCitizen(List<HouseHoldCitizenDto> dto, Long houseHoldId) {
		householdCitizenRepo.deleteByHouseHoldId(houseHoldId);
		List<HouseHoldCitizen> houseHoldCitizen = validateHouseCitizen(dto, houseHoldId);
		houseHoldCitizen = householdCitizenRepo.saveAll(houseHoldCitizen);
		return houseHoldCitizen;
	}
	private List<HouseHoldCitizen> validateHouseCitizen(List<HouseHoldCitizenDto> dto,Long houseHoldId) {
		List<HouseHoldCitizen> houseHoldCitizen = new ArrayList<HouseHoldCitizen>();
		for(int i= 0; i<dto.size();i++) {
			assertFalse(DataUtils.nullObject(dto.get(i)),"50000","Bạn đang để dòng trống");
			assertFalse(DataUtils.nullObject(dto.get(i).getCitizenId()),"50001","Bạn đang để trống ô chọn cá nhân!");
			if(dto.get(i).getStatusType().equals("TT")) {
				HouseHoldCitizen check = householdCitizenRepo.getByCitizenIdWithBeLong(dto.get(0).getCitizenId());
				HouseHold check2 = houseHoldRepo.getByCitizenId(dto.get(0).getCitizenId());
				assertFalse(!DataUtils.nullObject(check)||!DataUtils.nullObject(check2),"50002","Một cá nhân không thể thuộc 2 hộ khẩu!");

			}
			HouseHoldCitizen houseCitizen = new HouseHoldCitizen().builder().houseHoldId(houseHoldId)
                    .relationShipHouseHold(dto.get(i).getRelationShipHouseHold())
                    .citizenId(dto.get(i).getCitizenId())
                    .isactive(dto.get(i).getIsactive())
                    .statusType(dto.get(i).getStatusType())//thuộc hộ khẩu hay không ? có trường hợp tạm trú tạm vắng
                    .build();
			houseHoldCitizen.add(houseCitizen);
		}
		return houseHoldCitizen;
	}
}

