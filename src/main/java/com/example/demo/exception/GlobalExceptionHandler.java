package com.example.demo.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(RestApiException.class)
	public ResponseEntity<ErrorResponse> handlerRestApiException(RestApiException e){
		ErrorResponse errorResponse = new ErrorResponse(e.getErrorCode(),e.getMessage());
		return ResponseEntity.status(e.getHttpStatusCode()).body(errorResponse);
	}
}
