package com.example.demo.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class RestApiException  extends RuntimeException{

	private final String errorCode;
	private final int httpStatusCode;
	private boolean isErrorLevel = false;
	
	public RestApiException(String code, String message){
	     super(message);
	     this.errorCode = code ;
	     this.httpStatusCode = 400;
	}
	
}
