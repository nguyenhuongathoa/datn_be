package com.example.demo.controller;

import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.HouseHoldDto;
import com.example.demo.dto.NoticeDto;
import com.example.demo.dto.ResponseDto;
import com.example.demo.dto.request.HouseHoldRequestDto;
import com.example.demo.dto.response.HouseHoldCreatRespon;
import com.example.demo.dto.response.PagingDto;
import com.example.demo.entity.HouseHold;
import com.example.demo.entity.Notice;
import com.example.demo.repository.HouseHoldRepo;
import com.example.demo.service.HouseHoldService;

@RestController
@RequestMapping(BaseController.BASE_PATH_BACK + BaseController.BASE_PATH_HOUSEHOLD)
public class HouseHoldController extends BaseController{
	@Autowired
	private HouseHoldService houseHoldService;
	
	@GetMapping("/search")
	public ResponseEntity<ResponseDto<PagingDto>>getAllHouseHold(
			@RequestParam(name = "page", defaultValue = "1") @Min(0) int page,
			@RequestParam(name = "size", defaultValue = "5")@Min(1) int size,
			@RequestParam(name = "citizenNumber", defaultValue = "") String citizenNumber,
			@RequestParam(name = "searchValue", defaultValue = "") String searchValue,
			@RequestParam(name = "status",defaultValue = "") Boolean status){
		PagingDto result = houseHoldService.getAllHouseHold(page,size,searchValue,status,citizenNumber);
		return response(toResult(result)) ;

	}
	@GetMapping("/{id}")
	public  ResponseEntity<ResponseDto<HouseHold>>getfromId(@PathVariable Long id){
		HouseHold result = houseHoldService.getHouseHoldFromID(id);
		return response(toResult(result));
	}
	@PostMapping("/create")
	public ResponseEntity<ResponseDto<HouseHoldCreatRespon>> createHouseHold(@RequestBody HouseHoldRequestDto dto){
		HouseHoldCreatRespon houseHold = houseHoldService.createHouseHold(dto);
		return response(toResult(houseHold));
	}
	
	@PutMapping("/update")
	public ResponseEntity<ResponseDto<HouseHoldCreatRespon>> updateHouseHold(@RequestBody HouseHoldRequestDto dto){
		HouseHoldCreatRespon houseHold = houseHoldService.updateHouseHold(dto);
		return response(toResult(houseHold));
	}
	@DeleteMapping("/delete/{id}")
	public  ResponseEntity<ResponseDto<Boolean>> deleteHouseHold(@PathVariable Long id){
		Boolean result = houseHoldService.deleteHouseHold(id);
		return response(toResult(result));
	}
	
	

}
