package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ResponseDto;
import com.example.demo.entity.Notice;
import com.example.demo.entity.ReferenceData;
import com.example.demo.service.ReferenceDataService;

@RestController
@RequestMapping(BaseController.BASE_PATH_BACK + BaseController.BASE_PATH_REFERENCE)
public class ReferenceDataController extends BaseController {

	@Autowired
	private ReferenceDataService referenceDataServer;
	
	@GetMapping("")
	public ResponseEntity<ResponseDto<List<ReferenceData>>>getAllNotice(@RequestParam String type ){
		List<ReferenceData> listRef = referenceDataServer.getRefFromType(type);
		return response(toResult(listRef)) ;
		
	}

}
