package com.example.demo.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.demo.common.DataUtils;
import com.example.demo.dto.JwtInfoDto;
import com.example.demo.dto.ResponseDto;

public class BaseController {
	public static final String BASE_PATH_BACK = "/api/v1";

	public static final String BASE_PATH_NOTICE = "/notice";
	public static final String BASE_PATH_USER = "/user";
	public static final String BASE_PATH_REFERENCE = "/reference";
	public static final String BASE_PATH_HOUSEHOLD = "/house-hold";
	public static final String BASE_PATH_CITIZEN = "/citizen";
	public static final String BASE_PATH_TOTAL = "/total";
	public static final String BASE_PATH_CENSUS = "/census";


	

	protected <T> ResponseEntity<ResponseDto<T>> response(ResponseDto<T> response) {
		if (DataUtils.nullObject(response)) {
			throw new IllegalArgumentException("response empty");
		}

		return new ResponseEntity<ResponseDto<T>>(response, HttpStatus.OK);

	}

	protected <T> ResponseDto<T> toResult(T t) {
		ResponseDto<T> response = new ResponseDto<T>();
		response.setCode("200");
		response.setMessages("Success");
		response.setData(t);
		return response;
	}

	public JwtInfoDto getJwtInfo(String authorization) {
    	String [] cutString = authorization.split("Bearer");
    	String jwtBase64 = cutString.length==2 ? cutString[1] : "";
    	DecodedJWT jwt = JWT.decode(jwtBase64);
    	String userName = jwt.getClaims().get("userName") != null ? jwt.getClaims().get("userName").asString() : null;
    	String role = jwt.getClaims().get("role") != null ? jwt.getClaims().get("role").asString() : null;
    	
    	return JwtInfoDto.builder()
    			.role(role)
    			.userName(userName)
    			.build();
    }

}
