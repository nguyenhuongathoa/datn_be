package com.example.demo.controller;

import java.util.List;

import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.NoticeDto;
import com.example.demo.dto.ResponseDto;
import com.example.demo.dto.response.PagingDto;
import com.example.demo.entity.Notice;
import com.example.demo.service.NoticeService;

@RestController
@RequestMapping(BaseController.BASE_PATH_BACK + BaseController.BASE_PATH_NOTICE)
public class NoticeController extends BaseController {
	@Autowired
	private NoticeService noticeServer;
	
	@GetMapping("")
	public ResponseEntity<ResponseDto<PagingDto>>getAllNotice(
			@RequestParam(name = "page", defaultValue = "1") @Min(0) int page,
			@RequestParam(name = "size", defaultValue = "5")@Min(1) int size,
			@RequestParam(name = "type", defaultValue = "") String type,
			@RequestParam(name = "searchValue", defaultValue = "") String searchValue,
			@RequestParam(name = "status",defaultValue = "") String status,
			@RequestParam(name = "fromDate",defaultValue = "") String fromDate,
			@RequestParam(name = "toDate",defaultValue = "") String toDate){
		PagingDto result = noticeServer.getAllNotice(page, size,type,searchValue,status,fromDate, toDate);
		return response(toResult(result)) ;
		
	}
	
	@GetMapping("/new/{id}")
	public  ResponseEntity<ResponseDto<Notice>>getfromId(@PathVariable Long id){
		Notice result = noticeServer.getNoticeFromId(id);
		return response(toResult(result));
	}
	
	@PostMapping("/create")
	public ResponseEntity<ResponseDto<Notice>> createNotice(@RequestBody NoticeDto dto){
		Notice noticeEntity = noticeServer.CreateNotice(dto);
		return response(toResult(noticeEntity));
	}
	
	@PutMapping("/update")
	public ResponseEntity<ResponseDto<Notice>> updateNotice(@RequestBody Notice dto){
		Notice noticeEntity = noticeServer.updateNotice(dto);
		return response(toResult(noticeEntity));
	}
	
	@DeleteMapping("/delete/{id}")
	public  ResponseEntity<ResponseDto<Boolean>> deletNotice(@PathVariable Long id){
		Boolean result = noticeServer.deleteNotice(id);
		return response(toResult(result));
	}

}
