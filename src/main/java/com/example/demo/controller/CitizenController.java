package com.example.demo.controller;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CitizenDto;
import com.example.demo.dto.CitizenWithHouseHoldDto;
import com.example.demo.dto.NoticeDto;
import com.example.demo.dto.ResponseDto;
import com.example.demo.dto.response.PagingDto;
import com.example.demo.entity.Citizen;
import com.example.demo.entity.Notice;
import com.example.demo.service.CitizenService;

@RestController
@RequestMapping(BaseController.BASE_PATH_BACK + BaseController.BASE_PATH_CITIZEN)
public class CitizenController extends BaseController {

	@Autowired
	private CitizenService citizenService;

	@GetMapping("/search")
	public ResponseEntity<ResponseDto<PagingDto>> getAllNotice(
			@RequestParam(name = "page", defaultValue = "1") @Min(0) int page,
			@RequestParam(name = "size", defaultValue = "5") @Min(1) int size,
			@RequestParam(name = "ageTo", defaultValue = "0") @Min(0) @Max(160) int ageTo,
			@RequestParam(name = "ageFrom", defaultValue = "160") @Min(1) @Max(200) int ageFrom,
			@RequestParam(name = "sex", defaultValue = "") String sex,
			@RequestParam(name = "indentificationNumber", defaultValue = "") String indentificationNumber,
//			@RequestParam(name = "phoneNumber", defaultValue = "") String phoneNumber,
//			@RequestParam(name = "email", defaultValue = "") String email,
			@RequestParam(name = "religion", defaultValue = "") String religion,
			@RequestParam(name = "nation", defaultValue = "") String nation,
			@RequestParam(name = "education", defaultValue = "") String education,
			@RequestParam(name = "educationGraduate", defaultValue = "") String educationGraduate,
			@RequestParam(name = "speciallized", defaultValue = "") String speciallized,
			//@RequestParam(name = "job", defaultValue = "") String job,
			@RequestParam(name = "vaccination", defaultValue = "") String vaccination,
			@RequestParam(name = "searchValue", defaultValue = "") String searchValue,
			@RequestParam(name = "statusAlive", defaultValue = "") Boolean statusAlive) {
		PagingDto result = citizenService.getAllCitizens(page, size, searchValue, statusAlive, sex,
				indentificationNumber, religion, nation, education, educationGraduate, speciallized,
			 vaccination, ageTo, ageFrom);
		return response(toResult(result));

	}

	@GetMapping("/{id}")
	public ResponseEntity<ResponseDto<Citizen>> getCitizenDetail(@PathVariable Long id) {
		Citizen result = citizenService.getCitizenById(id);
		return response(toResult(result));
	}

	@PostMapping("/create")
	public ResponseEntity<ResponseDto<Citizen>> createCitizen(@RequestBody CitizenDto dto) {
		Citizen result = citizenService.createCitizen(dto);
		return response(toResult(result));
	}

	@PostMapping("/create-house-hold")
	public ResponseEntity<ResponseDto<Citizen>> createCitizenWithHouseHold(@RequestBody CitizenWithHouseHoldDto dto) {
		Citizen result = citizenService.createCitizenWithHouse(dto);
		return response(toResult(result));
	}

	@PutMapping("/update")
	public ResponseEntity<ResponseDto<Citizen>> updateCitizen(@RequestBody CitizenDto dto) {
		Citizen result = citizenService.updateCitizen(dto);
		return response(toResult(result));
	}

	@DeleteMapping("delete/{id}")
	public ResponseEntity<ResponseDto<Boolean>> deleteCitizen(@PathVariable Long id) {
		Boolean result = citizenService.deleteCitizen(id);
		return response(toResult(result));
	}
	
	@GetMapping("/list-for-host/{id}")
	public ResponseEntity<ResponseDto<List<Citizen>>> getCitizenForHost(@PathVariable Long id) {
		List<Citizen> result = citizenService.getCitizenForHost(id);
		return response(toResult(result));
	}
	//getCitizenForUser
	@GetMapping("/list-for-user")
	public ResponseEntity<ResponseDto<List<Citizen>>> getCitizenForUser() {
		List<Citizen> result = citizenService.getCitizenForUser();
		return response(toResult(result));
	}
}
