package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CitizenDto;
import com.example.demo.dto.ResponseDto;
import com.example.demo.entity.Census;
import com.example.demo.entity.Citizen;
import com.example.demo.service.CensusService;

@RestController
@RequestMapping(BaseController.BASE_PATH_BACK + BaseController.BASE_PATH_CENSUS)
public class CensusController extends BaseController{

	@Autowired
	private CensusService censusService;
	
	@GetMapping("/getAll")
	public ResponseEntity<ResponseDto<List<Census>>> getAllCensus() {
		List<Census> result = censusService.getAllCensus();
		return response(toResult(result));

	}
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<ResponseDto<Boolean>> deleteCitizen(@PathVariable Long id) {
		Boolean result = censusService.deleteCensus(id);
		return response(toResult(result));
	}
	
	@PostMapping("/create")
	public ResponseEntity<ResponseDto<Census>> createCitizen(@RequestBody Census dto) {
		Census result = censusService.saveCensus(dto);
		return response(toResult(result));
	}
	
	@GetMapping("/caculate")
	public ResponseEntity<ResponseDto<Census>> caculateData() {
		Census result = censusService.Caculate();
		return response(toResult(result));

	}

}
