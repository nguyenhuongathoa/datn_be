package com.example.demo.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.NoticeDto;
import com.example.demo.dto.ResponseDto;
import com.example.demo.dto.UsersDto;
import com.example.demo.dto.request.ChangPasswordDto;
import com.example.demo.dto.request.SinginInputDto;
import com.example.demo.dto.response.PagingDto;
import com.example.demo.dto.response.SingInOutputDto;
import com.example.demo.entity.Citizen;
import com.example.demo.entity.Notice;
import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@RestController
@RequestMapping(BaseController.BASE_PATH_BACK + BaseController.BASE_PATH_USER)
public class UserController extends BaseController {
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/singin")
	public ResponseEntity<ResponseDto<SingInOutputDto>> checkSingIn(@RequestBody SinginInputDto dto){
		SingInOutputDto  response= userService.singIn(dto);
		return response(toResult(response)) ;
	}
	@PostMapping("/create")
	public ResponseEntity<ResponseDto<User>> createUser(@RequestBody UsersDto dto){
		User response = userService.createUserNomal(dto);
		return response(toResult(response));
	}
	@PutMapping("/update")
	public ResponseEntity<ResponseDto<User>> updateUser(@RequestBody User dto){
		User response = userService.updateUser(dto);
     	return response(toResult(response));
	}
	@DeleteMapping("/delete/{userId}")
	public  ResponseEntity<ResponseDto<Boolean>> deletNotice(@PathVariable Long userId){
		Boolean result = userService.deleteUser(userId);
		return response(toResult(result));
	}
	@GetMapping("")
	public ResponseEntity<ResponseDto<PagingDto>> getCitizenDetail() {
		PagingDto result = userService.getListUsers();
		return response(toResult(result));
	}
	@PutMapping("/update-password")
	public ResponseEntity<ResponseDto<User>> updatePassword(@RequestBody ChangPasswordDto dto){
		User response = userService.updatePasswordUser(dto);
     	return response(toResult(response));
	}

}
