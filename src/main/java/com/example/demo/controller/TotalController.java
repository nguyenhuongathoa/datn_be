package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ResponseDto;
import com.example.demo.dto.response.CharPieDto;
import com.example.demo.entity.CitizenSexView;
import com.example.demo.service.TotalService;

@RestController
@RequestMapping(BaseController.BASE_PATH_BACK + BaseController.BASE_PATH_TOTAL)
public class TotalController extends BaseController {
	@Autowired
	private TotalService totalService;
	
	@GetMapping("/sex")
	public ResponseEntity<ResponseDto<CharPieDto>> getCitizenSexTotal() {
		CharPieDto result = totalService.getTotalSex();
		return response(toResult(result));
	}
	@GetMapping("/vaccin")
	public ResponseEntity<ResponseDto<CharPieDto>> getCitizenVaccinTotal() {
		CharPieDto result = totalService.getTotalVaccin();
		return response(toResult(result));
	}

	@GetMapping("/age")
	public ResponseEntity<ResponseDto<CharPieDto>> getCitizenAgeTotal() {
		CharPieDto result = totalService.getTotalAge();
		return response(toResult(result));
	}
	
	@GetMapping("/nation")
	public ResponseEntity<ResponseDto<CharPieDto>> getCitizenNationTotal() {
		CharPieDto result = totalService.getTotalNation();
		return response(toResult(result));
	}

	@GetMapping("/education")
	public ResponseEntity<ResponseDto<CharPieDto>> getCitizenEducationTotal() {
		CharPieDto result = totalService.getTotalEducation();
		return response(toResult(result));
	}
	
	@GetMapping("/religion")
	public ResponseEntity<ResponseDto<CharPieDto>> getCitizenReligionTotal() {
		CharPieDto result = totalService.getTotalReligion();
		return response(toResult(result));
	}

}
