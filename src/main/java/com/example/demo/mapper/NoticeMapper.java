package com.example.demo.mapper;

import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import com.example.demo.dto.NoticeDto;
import com.example.demo.entity.Notice;

@Component
@Mapper
public interface NoticeMapper extends iBaseMapper<NoticeDto,Notice>{

}
