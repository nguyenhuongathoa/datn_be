package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.Census;
import com.example.demo.entity.Citizen;

public interface CensusRepository extends JpaRepository<Census, Long>  {

	@Query(value = "SELECT * FROM CENSUS c order by create_date ASC", nativeQuery = true)
	List<Census> getAll();
	
	@Query(value = "SELECT * FROM CENSUS c WHERE c.CENSUS_ID = :id", nativeQuery = true)
	Census getCensusById(@Param(value = "id") Long id);
}
