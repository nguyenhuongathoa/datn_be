package com.example.demo.repository;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.HouseHold;
import com.example.demo.entity.HouseHoldView;
import com.example.demo.entity.Notice;

//chưa xong
//search mới chỉ theo địa chỉ và status
//bổ sung search theo tên người trong
@Repository
public class HouseHoldRepo {

	@Autowired
    private EntityManager entityManager;
 
 public List<HouseHoldView> search(int rowBegin, int rowEnd, String searchValue,Boolean status, String citizenNumber){
		StringBuilder qr = new StringBuilder();
	     qr.append("select * from (select h.*, row_number() over (ORDER BY h.CREATE_DATE DESC) RNUM ");
	     qr.append(this.getSQLQuery( searchValue,status,citizenNumber));
	     qr.append(") where RNUM >= :rowBegin and RNUM <= :rowEnd");
	     Query query = entityManager.createNativeQuery(qr.toString(), HouseHoldView.class);
	     query = query.setParameter("rowBegin", rowBegin);
	     query = query.setParameter("rowEnd", rowEnd);
		return query.getResultList();
	}
	public String getSQLQuery(String searchValue, Boolean status,String citizenNumber) {
		StringBuilder sb = new StringBuilder();
		sb.append(" from HOUSEHOLD_VIEW h Where 1 = 1");
		if(citizenNumber != null && !citizenNumber.equals("")) {
			sb.append(" and h.NUMBER_CITIZEN = '" + citizenNumber +"'" );
		}
		if(searchValue != null && !searchValue.equals("")) {
			sb.append(" and ( UPPER(h.GROUP_ADDRESS) like UPPER('%"+searchValue+"%') ");
			sb.append("or UPPER(h.HOST_NAME) like UPPER('%" + searchValue + "%') ");
			sb.append("or UPPER(h.MEMBER_NAME) like UPPER('%" + searchValue + "%') ");
			sb.append("or UPPER(h.ADDRESS) like UPPER('%" + searchValue + "%')) ");
		}
		if(status != null) {
			if(status == true){
				sb.append(" and h.STATUS = 1" );
			}else {
				sb.append(" and h.STATUS = 0" );
			}
		}
		return sb.toString();
		
	}
	public int countTotalHouseHold( String searchValue,Boolean status,String citizenNumber) {
		StringBuilder qr = new StringBuilder();
		qr.append("Select count (*) ");
		qr.append(this.getSQLQuery( searchValue,status,citizenNumber));
		Query query = entityManager.createNativeQuery(qr.toString());
		BigDecimal count = (BigDecimal)query.getSingleResult();
		return count.intValue();
	}
	
}
