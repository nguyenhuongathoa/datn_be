package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Citizen;
import com.example.demo.entity.CitizenSexView;

public interface CitizenSexViewRepository  extends JpaRepository<CitizenSexView, Long> {

}
