package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.CitizenReligionView;

public interface CitizenReligionViewRepository  extends JpaRepository<CitizenReligionView, Long>{

}
