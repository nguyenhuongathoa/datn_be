package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.CitizenNationView;

public interface CitizenNationViewRepository extends JpaRepository<CitizenNationView, Long>{

}
