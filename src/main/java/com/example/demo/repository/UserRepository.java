package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Notice;
import com.example.demo.entity.User;


public interface UserRepository extends JpaRepository<User, Long>{

	@Query(value= "SELECT * FROM USER_CITIZEN c WHERE c.USER_NAME = :userName and c.USER_PASSWORD = :userPassword" ,nativeQuery = true)
	User checkSingIn(@Param(value = "userName") String userName, @Param(value = "userPassword") String userPassword);
	
	@Query(value= "SELECT * FROM USER_CITIZEN c WHERE c.USER_NAME = :userName or c.CITIZEN_ID = :citizenId" ,nativeQuery = true)
	User checkUserExit(@Param(value = "userName") String userName, @Param(value = "citizenId") Long citizenId);
	
	@Query(value= "SELECT * FROM USER_CITIZEN c WHERE c.CITIZEN_ID = :citizenId" ,nativeQuery = true)
	User getByCitizenId(@Param(value = "citizenId") Long citizenId);
	
	@Query(value= "SELECT * FROM USER_CITIZEN c WHERE c.USER_ROLE like 'USER_%'" ,nativeQuery = true)
	List<User> getListCitizen();

	@Query(value= "SELECT c.CITIZEN_ID FROM USER_CITIZEN c" ,nativeQuery = true)
	List<Long> getlistCitizenId();
	
	
}
