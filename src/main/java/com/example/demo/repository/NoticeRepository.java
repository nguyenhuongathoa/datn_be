package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.Notice;
import com.example.demo.entity.User;

public interface NoticeRepository extends JpaRepository<Notice,Long>{
	@Query(value= "SELECT * FROM NOTICE n WHERE n.NOTICE_ID = :id" ,nativeQuery = true)
	Notice getById(@Param(value = "id") Long id);
}
