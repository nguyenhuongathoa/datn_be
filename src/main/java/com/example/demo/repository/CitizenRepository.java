package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.Citizen;
import com.example.demo.entity.Notice;

public interface CitizenRepository extends JpaRepository<Citizen, Long> {

	@Query(value = "SELECT * FROM CITIZEN c WHERE c.IDENTIFICATIONN_TYPE = :indentificationType and c.IDENTIFICATIONN_NUMBER = :indentificationNumber", nativeQuery = true)
	Citizen getByIndentification(@Param(value = "indentificationType") String indentificationType,
			@Param(value = "indentificationNumber") String indentificationNumber);
	
	@Query(value = "SELECT * FROM CITIZEN c WHERE c.CITIZEN_ID = :id", nativeQuery = true)
	Citizen getById(@Param(value = "id") Long id);

	@Query(value = "SELECT * FROM CITIZEN c WHERE c.CITIZEN_ID not in (:ids)", nativeQuery = true)
	List<Citizen> getListCitizenForAssign(@Param(value = "ids") List<Long> ids);
	
	
	@Query(value = "SELECT * FROM CITIZEN c WHERE c.STATUS_ALIVE = 1 ", nativeQuery = true)
	List<Citizen> getListCitizenAlive();

	
	
}
