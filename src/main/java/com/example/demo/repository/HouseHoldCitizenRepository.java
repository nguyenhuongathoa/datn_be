package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.Citizen;
import com.example.demo.entity.HouseHoldCitizen;

public interface HouseHoldCitizenRepository extends JpaRepository<HouseHoldCitizen, Long>{

	@Query(value = "SELECT * FROM HOUSEHOLD_CITIZEN hc WHERE hc.CITIZEN_ID = :id and hc.IS_ACTIVE = 1", nativeQuery = true)
	HouseHoldCitizen getByCitizenId(@Param(value = "id") Long id);
	
	@Query(value = "SELECT * FROM HOUSEHOLD_CITIZEN hc WHERE hc.CITIZEN_ID = :id and hc.STATUS_TYPE = 'TT'", nativeQuery = true)
	HouseHoldCitizen getByCitizenIdWithBeLong(@Param(value = "id") Long id);
	
	@Query(value = "SELECT * FROM HOUSEHOLD_CITIZEN hc WHERE hc.CITIZEN_ID = :id", nativeQuery = true)
	HouseHoldCitizen getByCitizenIdAlone(@Param(value = "id") Long id);
	
	@Query(value = "SELECT * FROM HOUSEHOLD_CITIZEN hc WHERE hc.HOUSEHOLE_ID = :houseHoldId and hc.IS_ACTIVE = 1", nativeQuery = true)
	List<HouseHoldCitizen> getByHouseHold(@Param(value = "houseHoldId") Long houseHoldId);
	
	@Modifying
	@Query(value = "DELETE FROM HOUSEHOLD_CITIZEN hc WHERE hc.HOUSEHOLE_ID = :houseHoldId", nativeQuery = true)
	void deleteByHouseHoldId(@Param(value = "houseHoldId") Long houseHoldId);
	
	
	@Query(value = "SELECT hc.CITIZEN_ID FROM HOUSEHOLD_CITIZEN hc WHERE hc.HOUSEHOLE_ID != :houseHoldId", nativeQuery = true)
	List<Long> getListForHost(@Param(value = "houseHoldId") Long houseHoldId);
	
	@Query(value = "SELECT hc.CITIZEN_ID FROM HOUSEHOLD_CITIZEN hc ", nativeQuery = true)
	List<Long> getListForHostnotID();
	
}
