package com.example.demo.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Notice;

@Repository
public class NoticeSearchRepo {
	 @Autowired
	    private EntityManager entityManager;
	 
	 public List<Notice> search(int rowBegin, int rowEnd, String type, String searchValue,String status,String fromDate, String toDate){
			StringBuilder qr = new StringBuilder();
		     qr.append("select * from (select n.*, row_number() over (ORDER BY n.CREATE_DATE DESC) RNUM ");
		     qr.append(this.getSQLQuery(type, searchValue,status,fromDate, toDate));
		     qr.append(") where RNUM >= :rowBegin and RNUM <= :rowEnd");
		     Query query = entityManager.createNativeQuery(qr.toString(), Notice.class);
		     query = query.setParameter("rowBegin", rowBegin);
		     query = query.setParameter("rowEnd", rowEnd);
			return query.getResultList();
		}
		public String getSQLQuery(String type, String searchValue, String status,String fromDate, String toDate) {
			StringBuilder sb = new StringBuilder();
			sb.append(" from NOTICE n Where 1 = 1");
			if(type != null && !type.equals("")) {
				sb.append(" and n.NOTICE_CATEGORY = '" + type +"'" );
			}
			if(searchValue != null && !searchValue.equals("")) {
				sb.append(" and ( UPPER(n.NOTICE_CONTENT) like UPPER('%"+searchValue+"%') ");
				sb.append("or UPPER(n.SUBJECT) like UPPER('%" + searchValue + "%')) ");
			}
			if(status != null && !status.equals("")) {
				sb.append(" and n.STATUS = '" + status +"'" );
			}
			if(fromDate != null && !fromDate.equals("")) {
				sb.append(" and n.CREATE_DATE >= to_timestamp('" + fromDate + "', 'YYYY-MM-DD')");
			}
			if(toDate != null && !toDate.equals("")) {
				try {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
					LocalDateTime dateTime1 = LocalDateTime.parse(toDate + " 12:30", formatter);
					LocalDateTime dateTime = dateTime1.plusDays(1);
					//Date date1 = new SimpleDateFormat("YYYY-MM-DD").parse(toDate);
					
					sb.append(" and n.CREATE_DATE <= to_timestamp('" + dateTime.getYear() + "-" + dateTime.getMonthValue()
					+ "-" + dateTime.getDayOfMonth() + "', 'YYYY-MM-DD')");
				}catch(Exception e) {
				  e.printStackTrace();
				}
				
			}

			return sb.toString();
			
		}
		public int countTotalNotice(String type, String searchValue,String status,String fromDate, String toDate) {
			StringBuilder qr = new StringBuilder();
			qr.append("Select count (*) ");
			qr.append(this.getSQLQuery(type, searchValue,status,fromDate, toDate));
			Query query = entityManager.createNativeQuery(qr.toString());
			BigDecimal count = (BigDecimal)query.getSingleResult();
			return count.intValue();
		}
		
}
