package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.CitizenAgeView;

public interface CitizenAgeViewRepository extends JpaRepository<CitizenAgeView, Long>{

}
