package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.ReferenceData;

public interface ReferenceDataRepository extends JpaRepository<ReferenceData,Long>{

	@Query(value= "SELECT * FROM REFERENCE_DATA r WHERE r.TYPE = :type" ,nativeQuery = true)
	List<ReferenceData> getReferenceFromType(@Param(value = "type") String type);
}
