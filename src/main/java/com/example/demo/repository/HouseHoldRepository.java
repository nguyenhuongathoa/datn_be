package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.HouseHold;
import com.example.demo.entity.Notice;

public interface HouseHoldRepository extends  JpaRepository<HouseHold,Long>{

	@Query(value= "SELECT * FROM HOUSEHOLD h WHERE h.HOUSEHOLD_ID = :id" ,nativeQuery = true)
	HouseHold getById(@Param(value = "id") Long id);
	
	@Query(value= "SELECT * FROM HOUSEHOLD h WHERE h.CITIZEN_ID = :citizenId and h.STATUS  = 1" ,nativeQuery = true)
	HouseHold getByCitizenId(@Param(value = "citizenId") Long citizenId);
	
	@Query(value= "SELECT h.CITIZEN_ID FROM HOUSEHOLD h WHERE  h.HOUSEHOLD_ID != :houseHoldId" ,nativeQuery = true)
	List<Long> getlistHost(@Param(value = "houseHoldId") Long houseHoldId);
	
	@Query(value= "SELECT h.CITIZEN_ID FROM HOUSEHOLD h " ,nativeQuery = true)
	List<Long> getlistHostWithNotId();
}
