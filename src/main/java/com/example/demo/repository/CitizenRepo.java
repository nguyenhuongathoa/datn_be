package com.example.demo.repository;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Citizen;
import com.example.demo.entity.CitizenHouseView;
import com.example.demo.entity.Notice;

@Repository
public class CitizenRepo {
	@Autowired
	private EntityManager entityManager;

	public List<CitizenHouseView> search(int rowBegin, int rowEnd, String searchValue, Boolean status, String sex,
			String indentification, String religion, String nation, String education, String educationGraduate,
			String speciallized,  String vaccination, int ageTo, int ageFrom) {
		StringBuilder qr = new StringBuilder();
		qr.append("select * from (select c.*, row_number() over (ORDER BY c.CREATE_DATE DESC) RNUM ");
		qr.append(this.getSQLQuery(searchValue, status, sex, indentification, religion, nation, education,
				educationGraduate, speciallized, vaccination, ageTo, ageFrom));
		qr.append(") where RNUM >= :rowBegin and RNUM <= :rowEnd");
		Query query = entityManager.createNativeQuery(qr.toString(), CitizenHouseView.class);
		// this.setAllParam(ageTo, ageFrom, query);

		query = query.setParameter("rowBegin", rowBegin);
		query = query.setParameter("rowEnd", rowEnd);
		return query.getResultList();
	}

	public String getSQLQuery(String searchValue, Boolean status, String sex, String indentification, String religion,
			String nation, String education, String educationGraduate, String speciallized,
			String vaccination, int ageTo, int ageFrom) {
		StringBuilder sb = new StringBuilder();
		sb.append(" from CITIZEN_HOUSE_VIEW c Where 1 = 1");
		if (sex != null && !sex.equals("")) {
			sb.append(" and c.SEX = '" + sex + "'");
		}
		if (indentification != null && !indentification.equals("")) {
			sb.append(" and (UPPER(c.IDENTIFICATIONN_NUMBER) like UPPER('%" + indentification + "%') ");
			sb.append("or UPPER(c.PHONE_NUMBER) like UPPER('%" + indentification + "%') ");
			sb.append("or UPPER(c.EMAIL) like UPPER('%" + indentification + "%') ");
			sb.append("or UPPER(c.PASSPORT) like UPPER('%" + indentification + "%')) ");
		}
		if (religion != null && !religion.equals("")) { //UPPER(c.NATION) like UPPER('%" + nation + "%')) "
			sb.append(" and UPPER(c.RELIGION) like UPPER('%" + religion + "%') ");
		}
		if (nation != null && !nation.equals("")) {
			sb.append(" and UPPER(c.NATION) like UPPER('%" + nation + "%') ");
		}
		if (education != null && !education.equals("")) {
			sb.append(" and c.EDUCATION = '" + education + "'");
		}
		if (educationGraduate != null && !educationGraduate.equals("")) {
			sb.append(" and c.EDUCATION_GRADUATE = '" + educationGraduate + "'");
		}
		if (speciallized != null && !speciallized.equals("")) {
			sb.append(" and (UPPER(c.SPECIALIZED) like UPPER('%" + speciallized + "%') ");
			sb.append("or UPPER(c.JOB) like UPPER('%" + speciallized + "%')) ");
		}
//		if (job != null && !job.equals("")) {
//			sb.append(" and UPPER(c.JOB) like UPPER('%" + job + "%') ");
//		}
		if (vaccination != null && !vaccination.equals("")) {
			sb.append(" and c.VACCINATION = '" + vaccination + "'");
		}
		if (searchValue != null && !searchValue.equals("")) {
			sb.append(" and ( UPPER(c.FULL_NAME) like UPPER('%" + searchValue + "%') ");
			sb.append("or UPPER(c.RESIDENT_ADDRESS) like UPPER('%" + searchValue + "%') ");
			sb.append("or UPPER(c.TEMPORARY_RESIDENCE_ADDRESS) like UPPER('%" + searchValue + "%') ");
			sb.append("or UPPER(c.REGION_BIRTH) like UPPER('%" + searchValue + "%') ");
			sb.append("or UPPER(c.HOST_NAME) like UPPER('%" + searchValue + "%') ");
			sb.append("or UPPER(c.COUNTRY_NAME) like UPPER('%" + searchValue + "%')) ");

		}
		if (status != null) {
			if (status == true) {
				sb.append(" and c.STATUS_ALIVE = 1");
			} else {
				sb.append(" and c.STATUS_ALIVE = 0");
			}
		}
		if (ageTo >= 0) {
			LocalDateTime local = LocalDateTime.now();
			// Date agedate = new
			// Date(local.getDayOfMonth(),local.getMonthValue(),local.getYear()-ageTo);
			sb.append(" and c.DATE_OF_BIRTH <= to_timestamp('" + (local.getYear() - ageTo) + "-" + local.getMonthValue()
					+ "-" + local.getDayOfMonth() + "', 'YYYY-MM-DD')");
			/// Date sqlDate = new java.sql.Date(utilDate.getTime())
		}
		if (ageFrom >= 0) {
			LocalDateTime local = LocalDateTime.now();
			// Date agedate = new
			// Date(local.getDayOfMonth(),local.getMonthValue(),local.getYear()-ageFrom);
			sb.append(" and c.DATE_OF_BIRTH >= to_timestamp('" + (local.getYear() - ageFrom) + "-"
					+ local.getMonthValue() + "-" + local.getDayOfMonth() + "', 'YYYY-MM-DD')");
			/// Date sqlDate = new java.sql.Date(utilDate.getTime())
		}
		return sb.toString();

	}

	public int countTotalCitizen(String searchValue, Boolean status, String sex, String indentification,
			String religion, String nation, String education, String educationGraduate, String speciallized,
			String vaccination, int ageTo, int ageFrom) {
		StringBuilder qr = new StringBuilder();
		qr.append("Select count (*) ");
		qr.append(this.getSQLQuery(searchValue, status, sex, indentification, religion, nation, education,
				educationGraduate, speciallized,vaccination, ageTo, ageFrom));
		Query query = entityManager.createNativeQuery(qr.toString());
		// this.setAllParam(ageTo, ageFrom, query);
		BigDecimal count = (BigDecimal) query.getSingleResult();
		return count.intValue();
	}
//	private void setAllParam(int ageTo, int ageFrom, Query qr) {
//		if(ageTo>=0) {
//			LocalDateTime local =LocalDateTime.now();
//		//	Date agedate = new Date(local.getYear()-ageTo,local.getMonthValue(),local.getDayOfMonth());
//		//	 Calendar calendar = Calendar.getInstance();
//			Timestamp ts = new Timestamp(local.getYear()-ageTo,local.getMonthValue(),local.getDayOfMonth(),0,0,0,0);
//			qr.setParameter("ageTo", ts);
//		}
//		
//		if(ageFrom>=0) {
//			LocalDateTime local =LocalDateTime.now();
//			Date agedate = new Date(local.getDayOfMonth(),local.getMonthValue(),local.getYear()-ageFrom);
//			
//			qr.setParameter("ageFrom", agedate);
//			///Date sqlDate = new java.sql.Date(utilDate.getTime())
//		}
//	}
}
