package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.CitizenEducationView;

public interface CitizenEducationViewRepository extends JpaRepository<CitizenEducationView, Long>{

}
